Commerce DocData Payments
------------------------
by Robert van Kemenade, robert@crc-online.nl


Description
-----------
This module handles iDeal payments through the DocData payment gateway.
The module needs to be checked and is currently being developed, more payment methods will be added.
If you wish to use this module verify it is working in your site before using it in production enviroments.

Installation 
------------
* Enable the module which can be found in the Commerce group of modules.
* Go to admin/commerce/config/payment-methods and enable the payment method
* On the DocData payment method click edit
* Edit the Action: DocData online payment