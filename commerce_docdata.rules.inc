<?php
/**
 * @file
 * Advanced rules for the docdata module to enable the processing of
 * orders.
 */

/**
 * Hook into HOOK_rules_action_info()
 *
 * Declare the actions that this module will add to the possible actions in the
 * rules module
 */
function commerce_docdata_rules_action_info() {
  $actions = array();
  return $actions;
}

/**
 * Hook into HOOK_rules_condition_info()
 *
 * Declare the conditions that this module will add to the possible conditions
 * in the rules module
 */
function commerce_docdata_rules_condition_info() {
  $conditions = array ();
  return $conditions;
}

/**
 * Hook into HOOK_rules_event_info()
 *
 * Declare the events that this module will add to the possible events
 * in the rules module
 */
function commerce_docdata_rules_event_info() {
  $events = array ();
  return $events;
}